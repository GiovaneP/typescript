export class Negociacao {
    //declaração das propriedades da classe (ts)
    /*O typescript precisa da declaração das propriedades

    Ao tornar estas propriedades private, não foi possível compilar porque no arquivo app.ts 
        está com uma atribuição na propriedade _quantidade, o que não é permitido.

    ': type' é a forma de atribuir tipo a uma variavel,parametro,propriedade, metodo (...)
    */
    /*
    Aula 04
    private _data : Date;
    private _quantidade : Number;
    private _valor : number;
     Typescript permite a geração de propriedades a partir da definição previa no construtor. Exemplo abaixo
    Para não ocorrer erro de compilação, é necessário não haver nenhuma declaração de propriedade na classe
    bem como não pode haver atribuiçao de valor no constructor
    */
   //propriedades readonly nao precisam de getters e dao erro com setters
    constructor(readonly data : Date,readonly  quantidade : number,readonly valor : number){
        /*por convenção, não se altera propriedades começados com _ fora da classe, 
        por isso existem metodos de acesso aos mesmos 
        
        regra de negocio --
        Uma negociação não pode ser alterada depois de criada; *por isso a convenção acima e apenas os metodos get*
         if(!data){
            throw new Error('data deve ser preenchida');
        }
        if(!quantidade){
            throw new Error('quantidade deve ser preenchida');
        }
        if(!valor){
            throw new Error('valor deve ser preenchido');
        } */
        //this._data = data;
        //this._quantidade = quantidade;
        //this._valor = valor;
    }

    get volume(){
        return this.quantidade * this.valor;
    }
}