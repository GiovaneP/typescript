import { NegociacaoController } from './controllers/NegociacaoController';

const controller = new NegociacaoController();
$('.form').submit(controller.adiciona.bind(controller));
// const negociacao = new Negociacao(new Date(),1,100);
// //console.log(negociacao);
// //console.log(negociacao.volume); -- ok
// /* const negociacao = new Negociacao(); */
// //console.log(negociacao);
// /* negociacao.quantidade = 3;*/
// //negociacao._quantidade = 3; // not ok // erro em tempo de compilação porque a variavel é private;

// //console.log(negociacao.valor);
// /*
// * A PARTIR DAQUI, SEGUNDO CAPITULO.
// */

// // const controller = new NegociacaoController();

// // document.querySelector('.form').addEventListener('submit', controller.adiciona.bind(controller));
// const controller = new NegociacaoController();

// $('.form').submit(controller.adiciona.bind(controller));